# README #

The WhatIf project is supported by the Science and Engineering Research Council from 2012 to 2015 through grants EP/J014354/1 and EP/J014176/1.

### What is this repository for? ###

* This is a stable version of that whatif... ontology authoring interface.
* Version 1.8
* Please study the manual or look at the wiki page for more information.

### How do I get set up? ###

* You need to have the latest version of JVM, which can be downloaded from the following link:
http://www.oracle.com/technetwork/java/javase/downloads/ jre8-downloads-2133155.html
* After installing JVM, open the "jar" folder and double click on the "whatif.jar" file. 
* You need to study the manual for operating the interface.

### Contribution guidelines ###

* If you would like any part of the code or would like to contribute to this project please let us know.

### Who do I talk to? ###

* If you encounter any issues, please either log an issue on this website or contact (a.parvizi@abdn.ac.uk)